from django.forms import ModelForm
from models import Thread, Post
from django import forms

class ThreadForm(ModelForm):
    class Meta:
        model = Thread
        fields = ['name', 'image', 'body', 'board']

    def clean_image(self):
         image = self.cleaned_data.get('image',False)
         if image:
             if image._size > 4*1024*1024:
                   raise ValidationError("Image file too large ( > 4mb )")
             return image
         else:
             raise ValidationError("Couldn't read uploaded image")


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ['name', 'image', 'body', 'thread']

    image = forms.ImageField(required=False)
    
    def clean_image(self):
        image = self.cleaned_data.get('image',False)
        if image:
            if image._size > 4*1024*1024:
                raise ValidationError("Image file too large ( > 4mb )")
            return image
        else:
            return None
