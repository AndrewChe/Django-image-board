# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from board.models import Board, Thread, Post, Category


# Register your models here.
@admin.register (Board)
class BoardAdmin (admin.ModelAdmin):
    pass

@admin.register (Thread)
class ThreadAdmin (admin.ModelAdmin):
    pass

@admin.register (Post)
class PostAdmin (admin.ModelAdmin):
    pass

@admin.register (Category)
class CategoryAdmin (admin.ModelAdmin):
    pass
