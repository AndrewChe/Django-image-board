# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, HttpResponse, Http404, redirect
from django.core.paginator import Paginator
from board.models import Board, Post, Thread, Category
from django.core.exceptions import ObjectDoesNotExist
from forms import ThreadForm, PostForm
from django.shortcuts import get_object_or_404

# Create your views here.



def board (request, url):

    if url == 'admin':
        return admin_board(request)

    if request.method == 'POST':
        new_thread = ThreadForm(request.POST, request.FILES)
        board = get_object_or_404(Board, url=url)
        if new_thread.is_valid():
            new_thread = new_thread.save(commit=False)
            new_thread.board = board
            new_thread.save()

    board = get_object_or_404(Board, url=url)
    threads = Thread.objects.filter(board=board.board_id).order_by('-thread_id')
    return render(request, 'board.html', {'board' : board,
                                          'threads' : threads})


def admin_board (request):
    board = Board.objects.get(url='admin')
    threads = Thread.objects.filter(board=board.board_id).order_by('-thread_id')
    return render(request, 'admin_board.html', {'board' : board,
                                          'threads' : threads})




def thread (request, id):
    if request.method == 'POST':
        new_post = PostForm(request.POST, request.FILES)
        thread = Thread.objects.get(thread_id=int(request.POST['thread']))
        if new_post.is_valid():
            new_post = new_post.save(commit=False)
            new_post.thread = thread
            new_post.save()

    thread = Thread.objects.get(thread_id=id)
    posts = Post.objects.filter(thread=thread.thread_id).order_by('post_id')
    return render(request, 'thread.html', {'thread' : thread,
                                            'posts' : posts})

def main (request):
    categories = list(Category.objects.all().values())
    boards = Board.objects.all()
    board_list = []
    for category in categories:
        board_list.append(category)
        board_list[-1]['type'] = 'category'
        board_items = list(boards.filter(category=category['category_id']).values())
        for board_item in board_items:
            board_item['type'] = 'board'
        board_list += board_items
    board_list = Paginator(board_list, 10)
    num_pages = range(board_list.num_pages)

    return render(request, 'index.html', {'board_list' : board_list,
                                          'num_pages' : num_pages})
