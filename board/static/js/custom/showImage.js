function showImage (image) {
  $('#show-image').attr('src', image);
  $('#image-modal').modal('show');
}

$(document).ready(function () {
  $('#my-imageupload').imageupload({
      allowedFormats: [ 'jpg', 'jpeg', 'png', 'gif' ],
      maxFileSizeKb: 4096
  });

});
