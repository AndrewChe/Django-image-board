# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Category (models.Model):
    category_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20)
    is_adults = models.BooleanField(default=False)
    def __unicode__(self):
        return self.name

class Board (models.Model):
    board_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20)
    url = models.CharField(max_length=5)
    category = models.ForeignKey('Category')
    description = models.CharField(max_length=100)
    def __unicode__(self):
        return self.name

class Thread (models.Model):
    thread_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20)
    image = models.ImageField(upload_to='pictures/', null=False)
    body = models.TextField(max_length=50000)
    board = models.ForeignKey ('Board')
    def __unicode__(self):
        return self.name


class Post (models.Model):
    post_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20)
    image = models.ImageField(upload_to='pictures/', null=True)
    body = models.TextField(max_length=10000)
    thread = models.ForeignKey ('Thread')
    parent = models.ForeignKey ('Post', null=True)
    def __unicode__(self):
        return self.name
