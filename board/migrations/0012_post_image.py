# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-21 08:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('board', '0011_auto_20170921_0825'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='image',
            field=models.ImageField(null=True, upload_to='pictures/'),
        ),
    ]
