# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, HttpResponse, redirect
from django.http import JsonResponse

from board.models import Board, Thread, Post

# Create your views here.



def board_api (request):
    res = {}
    if request.method == 'GET':
        offset = 0
        limit = 10
        if 'offset' in request.GET:
            offset = request.GET['offset']

        if 'limit' in request.GET:
            limit = request.GET['limit']

        if 'name' in request.GET:
            res = Board.objects.filter(name=request.GET['name'])
        else:
            res = Board.objects.all()[offset:limit]
    res = list(res.values())
    return JsonResponse({'res':res})

def thread_api (request):
    res = {}
    if request.method == 'GET':
        offset = 0
        limit = 10
        if 'offset' in request.GET:
            offset = request.GET['offset']

        if 'limit' in request.GET:
            limit = request.GET['limit']

        if 'name' in request.GET:
            res = Thread.objects.filter(name=request.GET['name'])
        else:
            res = Thread.objects.all()[offset:limit]
    res = list(res.values())
    return JsonResponse({'res':res})

def post_api (request):
    res = {}
    if request.method == 'GET':
        offset = 0
        limit = 10
        if 'offset' in request.GET:
            offset = request.GET['offset']

        if 'limit' in request.GET:
            limit = request.GET['limit']

        if 'name' in request.GET:
            res = Post.objects.filter(name=request.GET['name'])
        else:
            res = Post.objects.all()[offset:limit]
    res = list(res.values())
    return JsonResponse({'res':res})
