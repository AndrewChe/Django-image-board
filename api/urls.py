from django.conf.urls import url, include
from django.contrib import admin
from api import views

urlpatterns = [
    url(r'^board', views.board_api),
    url(r'^thread', views.thread_api),
    url(r'^post', views.post_api),
]
