from django import template

register = template.Library()

def get_page (value, num):
    return value.page(num+1)

register.filter('get_page', get_page)
