# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-30 08:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('board', '0013_auto_20170930_0707'),
    ]

    operations = [
        migrations.AlterField(
            model_name='thread',
            name='image',
            field=models.ImageField(upload_to='pictures/'),
        ),
    ]
