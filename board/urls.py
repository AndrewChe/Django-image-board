from django.conf.urls import url, include
from django.contrib import admin
from board import views
from api import urls

threads = [
    url(r'^(?P<id>\d+)$', views.thread)
]

urlpatterns = [
    #url(r'^api/', include(urls)),
    url(r'^thread/', include(threads)),
    url(r'^(?P<url>\w+)$', views.board),
    url(r'^$', views.main)
]
